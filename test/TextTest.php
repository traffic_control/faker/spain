<?php

namespace Faker\Test\Spain;

use Faker\Generator;
use Faker\Spain\Text;
use PHPUnit\Framework\TestCase;

final class TextTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    protected function setUp(): void
    {
        $faker = new Generator();
        $faker->addProvider(new Text($faker));
        $this->_faker = $faker;
    }

    public function testText()
    {
        $this->assertNotSame('', $this->_faker->realtext(200, 2));
    }
}
