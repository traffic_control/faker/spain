<?php

namespace Faker\Spain;

use Faker\Extension\Extension;

class PhoneNumber extends \Faker\Provider\PhoneNumber implements Extension
{
    protected static $formats = [
        '+34 9## ## ####',
        '+34 9## ######',
        '+34 9########',
        '+34 9##-##-####',
        '+34 9##-######',
        '9## ## ####',
        '9## ######',
        '9########',
        '9##-##-####',
        '9##-######',
    ];

    protected static $mobileFormats = [
        '+34 6## ## ####',
        '+34 6## ######',
        '+34 6########',
        '+34 6##-##-####',
        '+34 6##-######',
        '6## ## ####',
        '6## ######',
        '6########',
        '6##-##-####',
        '6##-######',
    ];

    protected static $tollFreeFormats = [
        '900 ### ###',
        '800 ### ###',
    ];

    public static function mobileNumber()
    {
        return static::numerify(static::randomElement(static::$mobileFormats));
    }

    public static function tollFreeNumber()
    {
        return static::numerify(static::randomElement(static::$tollFreeFormats));
    }
}
