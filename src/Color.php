<?php

namespace Faker\Spain;

class Color extends \Faker\Provider\Color
{
    protected static $safeColorNames = [
        'amarillo',
        'azul marino',
        'azul',
        'blanco',
        'celeste',
        'gris',
        'lima',
        'magenta',
        'marrón',
        'morado',
        'negro',
        'plata',
        'turquesa',
        'verde',
        'verde oliva',
    ];
}
